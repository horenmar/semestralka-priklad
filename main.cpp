#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>
#include <string>
#include <cstring>


namespace timer {
auto now() {
    return std::chrono::high_resolution_clock::now();
}

template <typename TimePoint>
long long to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
}

}

int st_estimation(int num_iters) {
    std::mt19937_64 rng(std::random_device{}());
    std::uniform_real_distribution<> dist;
    int inside = 0;
    for (int iter = 0; iter < num_iters; ++iter) {
        auto x = dist(rng);
        auto y = dist(rng);
        if (x*x + y*y <= 1) {
            inside++;
        }
    }
    return inside;
}

int mt_estimation(int num_estimates, int num_threads) {
    std::atomic<int> total_inside{ 0 };
    std::vector<std::thread> threads;
    for (int t = 0; t < num_threads; ++t) {
        threads.emplace_back([&]() {
            // Assume that num_estimates is divisible.
            total_inside += st_estimation(num_estimates / num_threads);
        });
    }
    for (auto& t : threads) {
        t.join();
    }
    return total_inside;
}

void print_usage(std::string const& exe_name) {
    std::clog << "Usage: " << exe_name << " num_iterations\n";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

int main(int argc, char** argv) {
    int num_iters = 0;
    int num_threads = std::thread::hardware_concurrency();
    
    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }
    
    if (argc == 1) {
        std::clog << "No arguments given, using default value of 10M iterations.\n";
        num_iters = 10'000'000;
    } else if (argc >= 3) {
        std::clog << "Too many arguments given.\n";
        print_usage(argv[0]);
        return 1;
    } else {
        std::size_t endpos = 0;
        num_iters = std::stoi(argv[1], &endpos);
        if (endpos != std::strlen(argv[1])) {
            std::clog << "The second argument, '" << argv[1] <<  "', is not a valid number.\n";
            return 2;
        }
    }

    auto t1 = timer::now();
    auto st_inside = st_estimation(num_iters);
    auto t2 = timer::now();
    auto mt_inside = mt_estimation(num_iters, num_threads);
    auto t3 = timer::now();

    std::cout << "ST inside points: " << st_inside << '\n';
    std::cout << "MT inside points: " << mt_inside << '\n';
    std::cout << "PI value: " << M_PI << '\n';
    std::cout << "ST PI time needed: " << timer::to_ms(t2-t1) << "ms value: " << 4. * st_inside / num_iters << '\n';
    std::cout << "MT PI time needed: " << timer::to_ms(t3-t2) << "ms value: " << 4. * mt_inside / num_iters << '\n';
}